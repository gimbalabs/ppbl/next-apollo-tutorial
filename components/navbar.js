import Link from 'next/link'
import { Flex, Spacer } from '@chakra-ui/react'

export default function Navbar() {
  return (
      <Flex direction="row" w="100%" p="5" bg="blue">
        <Spacer />
        <Link href="/">Home</Link>
        <Spacer />
        <Link href="/ssg">SSG</Link>
        <Spacer />
        <Link href="/isr">ISR</Link>
        <Spacer />
        <Link href="/ssr">SSR</Link>
        <Spacer />
        <Link href="/csr">Client Side</Link>
        <Spacer />
      </Flex>
  )
}