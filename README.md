This is a Next.js project.

It follows this tutorial: https://blog.logrocket.com/why-use-next-js-apollo/

And shows examples of how to use SSG, ISR, SSR and CSR with Next.js, Apollo Client, GraphQL and Dandelion.

## Next Steps:
Implement address table components (again). Make sure they work!