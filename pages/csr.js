import Head from "next/head";
import ClientOnly from "../components/ClientOnly";
import AddressTable from "../components/addressTable";
import { Box, Heading } from "@chakra-ui/react";

export default function ClientSide() {
  return (
    <Box>
      <Head>
        <title>Past SpaceX Launches</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box>
        <Heading>A Simple Example of Client Side Rendering</Heading>
        <ClientOnly>
          <AddressTable />
        </ClientOnly>
      </Box>
    </Box>
  );
}