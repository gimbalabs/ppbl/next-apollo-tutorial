// pages/_app.js
import { ApolloProvider } from "@apollo/client";
import client from "../apollo-client";
import { ChakraProvider } from '@chakra-ui/react'
import Navbar from '../components/navbar'

// ApolloProvider is added for use with Client Side Rendering
// See: https://blog.logrocket.com/why-use-next-js-apollo/#fetching-data-apollo-client-ssr-pages

function MyApp({ Component, pageProps }) {
  return (
    <ApolloProvider client={client}>
      <ChakraProvider>
        <Navbar />
        <Component {...pageProps} />
      </ChakraProvider>
    </ApolloProvider>
  )
}

export default MyApp